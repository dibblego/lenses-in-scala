\section{Lens Libraries}
\label{sect:lenslibraries}

So far, we have visited a few useful libraries on lenses:

\begin{itemize}
\item The \lstinline$modify$ function, available on any \lstinline$Lens$.
\item The \lstinline$compose$ function \textemdash a consequence of \lstinline$Lens$ being an instance of a category.
\item The \lstinline$fold$ function \textemdash a consequence of all categories being an instance of a monoid.
\item The \lstinline$Comonad$ instance of the \lstinline$CoState$ data structure.
\item The \lstinline$coFlatten$ function \textemdash a consequence of \lstinline$CoState$ being an instance of a comonad.
\end{itemize}

However, this list is relatively brief as the library that can be derived from the uses of lenses is very rich. Let us explore some of those libraries.

\subsection{Lens Product Morphism}
\label{subsect:lensproduct}

Let us take two disjoint lenses\footnote{That is, unrelated lenses with completely different types.} and combine them to produce a new lens by pairing their input and output {$(R \rightsquigarrow F) \rightarrow (S \rightsquigarrow G) \rightarrow (R \otimes S \rightsquigarrow F \otimes G)$}. This is useful for building up lenses into products of lenses. The type of this function is 
\begin{lstlisting}[label=lst:Lens_product_type,language=Scala]
Lens[R, F]
=> Lens[S, G]
=> Lens[(R, S), (F ,G)]
\end{lstlisting} and is implemented as an instance method on \lstinline$Lens[R, F]$.
\lstinputlisting[label=lst:Lens_product,caption=Taking the product of two lenses to produce a new lens]{source/LensProduct.scala}

\newpage

\subsection{Lens Choice Morphism}
\label{subsect:lenschoice}

We may take two lenses that view the same field type and join them into a choice on the record {$(R \rightsquigarrow F) \rightarrow (S \rightsquigarrow F) \rightarrow (R \oplus S \rightsquigarrow F)$}.

The type of this function is 
\begin{lstlisting}[label=lst:Lens_choice_type,language=Scala]
Lens[R, F]
=> Lens[S, F]
=> Lens[Either[R, S], F]
\end{lstlisting} and is implemented as an instance method on \lstinline$Lens[R, F]$.
\lstinputlisting[label=lst:Lens_choice,caption=Taking two lenses to choose on the record type to produce a new lens viewing the same field type]{source/LensChoice.scala}

\subsection{Lens Codiagonal Morphism}
\label{subsect:lenscodiag}

A lens gives rise to a codiagonal morphism {$A \oplus A \rightsquigarrow A$}. This may be thought of as taking either A or A and stripping the choice of A from the \lstinline$Either$ value.
The type of this value is \lstinline$Lens[Either[A, A], A]$.
\lstinputlisting[label=lst:Lens_codiag,caption=Lens is codiagonal]{source/LensCodiag.scala}

\subsection{Standard Lenses}
\label{subsect:standardlenses}

Some lenses are relatively mundane, operating on standard library data types. For example, the lens to get/set the first or second side of a pair:
\begin{lstlisting}[label=lst:Lens_pair,caption=Lens on Pairs,language=Scala]
def first[A, B]: Lens[(A, B), A] =
  Lens(_._1, (ab, a) => (a, ab._2))
def second[A, B]: Lens[(A, B), B] =
  Lens(_._2, (ab, b) => (ab._1, b))
\end{lstlisting}

Others operate on collections, such as \lstinline$Map$ or \lstinline$Set$:
\begin{lstlisting}[label=lst:Lens_MapSet,caption=Map and Set lenses,language=Scala]
def mapL[K, V](k: K)
    : Lens[Map[K, V], Option[V]] =
  Lens(
    _ get k
  , (m, v) => v match {
      case None => m - k
      case Some(w) => m + ((k, w))
    }
  )

def setL[K](k: K)
    : Lens[Set[K], Boolean] =
  Lens(
    _ contains k
  , (s, p) => if(p) s + k else s - k
  )
\end{lstlisting}

Several other standard lenses operate on:
\begin{itemize}
\item queues \textemdash enqueue, dequeue and length operations.
\item arrays \textemdash setting and retrieving a value at an index.
\item numeric values \textemdash performing numeric operations.
\end{itemize}

\subsection{Infix Type Alias}
\label{infixTypeAlias}

Since \lstinline$Lens$ is a binary type constructor and Scala has support for infix types\footnote{Scala Language Specification 3.2.8 Infix Types}, a lens library may benefit from an infix type alias. For example, the alias \lstinline$@>$ may be used to denote a lens and its asymmetry.

\begin{lstlisting}
type @>[A, B] = Lens[A, B]
\end{lstlisting}

This type alias is purely aesthetic and offers no computational power, however, some expressions may benefit from this aesthetic improvement. For example, consider the type of lens composition:

\begin{lstlisting}[label=lst:Lens_compose_infix,language=Scala]
(B @> C) => (A @> B) => (A @> C)
\end{lstlisting}

This type signature is similar to the type of regular function composition:

\begin{lstlisting}[label=lst:Function_compose_infix,language=Scala]
(B => C) => (A => B) => (A => C)
\end{lstlisting}

Indeed, the signature is the type of composition under a category, where that category is \lstinline$@>$ denoting the lens category in this instance and the \lstinline$=>$ category in the case of function composition.

\subsection{Lens Produces State}
\label{subsect:lensstate}

A lens gives rise to a trivial \lstinline$State$ instance by 
\begin{lstlisting}[label=lst:Lens_State,language=Scala]
l => State(s => (l get s, s))
\end{lstlisting}
Since \lstinline$State$ forms a monad, this gives rise to other useful programming constructs, such as emulating imperative programming (\ref{emulatingimperativeprogramming}).

\subsection{Emulating Imperative Programming}
\label{emulatingimperativeprogramming}

Lenses, combined with state and its monad provides a facility to emulate imperative programming without losing the program properties that allow us to reason equationally about our program i.e. we maintain referential transparency. Following is the set-up of a use-case by introducing a small subset of the typical lens/state libraries.

Let us first add two methods to lenses:
\begin{enumerate}
\item \lstinline$+=$ which operates on any numeric field of a record by adding a given value and boxing that computation in \lstinline$State$.
\item \lstinline$:=$ which updates the field of a record and boxing that computation in \lstinline$State$.
\end{enumerate}

\lstinputlisting[label=lst:Lens_imperative,caption=Lenses with methods for imperative programming]{source/LensImperative.scala}

Next, we examine some combinators on \lstinline$State$, which will be useful for our use-case:
\begin{enumerate}
\item \lstinline$get$ which gets the current state value into the non-state value of \lstinline$State$. That is, it duplicates the state value with \lstinline$s => (s, s)$.
\item \lstinline$eval$ which executes the \lstinline$State$ and discards the resulting state value, leaving only the non-state value.
\item \lstinline$st$ which implicitly lifts any lens into a \lstinline$State$ value \textemdash the trivial one mentioned earlier (\ref{subsect:lensstate}).
\end{enumerate}

\begin{lstlisting}[label=lst:State_combinators,caption=State combinator functions,language=Scala]
def get[S]: State[S, S] =
  State(s => (s, s))
def eval[S, A](t: State[S, A])(s: S): A =
  t.run(s)._1
implicit def st[R, F]
  (l: Lens[R, F])
  : State[R, F] =
    State(s => (l get s, s))
\end{lstlisting}
  
Let us now define the record on which we will deploy our use-case:

\begin{lstlisting}[label=lst:Employee,language=Scala]
case class Employee(
  name: String
, salary: Int
, age: Int
)
\end{lstlisting}

Now, we must hand-roll our lenses for each field of the employee record. We will look at techniques for integrating lenses into Scala so to avoid the need for this boiler-plate in section \ref{sect:existingwork}.
\begin{lstlisting}[label=lst:Employee_lenses,language=Scala]
val name =
  Lens(
    _.name
  , (e, n) => e copy (name = n)
  )
val salary =
  Lens(
    _.salary
  , (e, s) => e copy (salary = s)
  )
val age =
  Lens(
    _.age
  , (e, a) => e copy (age = a)
  )
\end{lstlisting}

\newpage

Finally, we demonstrate the use-case by:
\begin{itemize}
\item updating an employee's salary by \lstinline$100$
\item updating an employee's name by appending a surname \lstinline$" Jones"$
\end{itemize}

We do this by keeping these modifications inside a state value, which is constructed by composing smaller state values with its monad. The final modification is kept in a value, which may then be applied to any \lstinline$Employee$ instance.

\begin{lstlisting}[label=lst:ImperativeProgramming,caption=Imperative programming with lenses and state,language=Scala]
val modification =
  for {
    _ <- salary += 100
    n <- name
    _ <- name := n + " Jones"
    e <- get
  } yield e

val bill = Employee("Bill", 1100, 33)
val updatedBill = eval(modification)(bill)
println(updatedBill)
\end{lstlisting}

The above program prints:

\begin{lstlisting}[label=lst:ImperativeProgrammingResult,language=Scala]
> Employee("Bill Jones", 1200, 33)
\end{lstlisting}

We note that the employee has had the modifications performed without in-place updates and by combining small, reusable program parts to produce the use-case. With sufficient library support, and higher-level abstractions, non-trivial use-cases can be easily and robustly handled.

This example, and all associated code mentioned so far, is available to download and run from Github \cite{Code}.

