\section{The Problem}
\label{sect:theproblem}

A trivial use-case to demonstrate the boilerplate problem is that of defining a \emph{modify} function for a given field of a record. The modify function is used to pass a function and a record and it returns a new record having updated the field with that function. The type signature of the modify function takes on the form:

\begin{lstlisting}[label=lst:modifysignature,language=Scala, caption=The signature of a \lstinline$modify$ function]
def modify: (Field => Field) => Rec => Rec
\end{lstlisting}

For example, taking the running example of the \lstinline$age$ field of the \lstinline$Person$ record\footnote{This definition could also be written as an instance method of the \lstinline$Person$ class.}:

\begin{lstlisting}[label=lst:modifyAge,language=Scala, caption=The \lstinline$modifyAge$ function]
def modifyAge
  (f: Int => Int)
  (p: Person)
  : Person =
    p.copy(age = f(p.age))
\end{lstlisting}

A subsequent definition of a modify function for the \lstinline$address$ field of the \lstinline$Person$ class would require similar boilerplate code:

\begin{lstlisting}[label=lst:modifyAddress,language=Scala, caption=The \lstinline$modifyAddress$ function]
def modifyAddress
  (f: Address => Address)
  (p: Person)
  : Person =
    p.copy(address = f(p.address))
\end{lstlisting}

Indeed, there are potentially as many modify functions as there are record fields, each requiring programmer intervention to implement.

However, the problem is even more pronounced. Let us suppose we wish to access the \lstinline$street$ field of the \lstinline$address$ field of a \lstinline$Person$.

\begin{lstlisting}[label=lst:getPersonStreet,language=Scala, caption=The \lstinline$getPersonStreet$ function]
def getPersonStreet(p: Person): String =
  p.address.street
\end{lstlisting}

Similarly, we may wish to update the \lstinline$street$ field of the \lstinline$address$ field of a \lstinline$Person$.

\begin{lstlisting}[label=lst:setPersonStreet,language=Scala, caption=The \lstinline$setPersonStreet$ function]
def setPersonStreet
  (p: Person)
  (s: String)
  : String =
    p.copy(address = 
      p.address.copy(street = s))
\end{lstlisting}

There is also another potential modify function for a Person's (indirect) street field. This particular undesirable consequence is not experienced when modelling the program design to use mutable fields, however, if we are to strive for the benefits of immutable records, we need to find a way to alleviate this issue of laborious boilerplate code.

In summary, the problem grows proportional to $M \times N$ where:

\begin{itemize}
\item $M =$ The number of potential record fields.
\item $N =$ The number of potential library functions on record fields in general.\footnote{We will go further into these library functions later in section \ref{sect:lenslibraries}.}
\end{itemize}

A cursory examination of a record's generated accessor and copy method gives us functions with the types:
\begin{itemize}
\item \lstinline$get: Rec => Field$
\item \lstinline$set: (Rec, Field) => Rec$
\end{itemize}

For example, the \lstinline$age$ accessor and copy method have the types:
\begin{itemize}
\item \lstinline$Person => Int$
\item \lstinline$(Person, Int) => Person$
\end{itemize}

We will now take this observation and explore its utility in detail.

