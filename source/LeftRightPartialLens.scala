def leftPartialLens[A, B]
    : PartialLens[Either[A, B], A] =
  PartialLens {
  	case Left(a)  => 
  	  Some(CoState(a, Left(_)))
  	case Right(_) => 
  	  None
  }
  
def rightPartialLens[A, B]
    : PartialLens[Either[A, B], B] =
  PartialLens {
  	case Right(b) => 
  	  Some(CoState(b, Right(_)))
  	case Left(_)  => 
  	  None
  }