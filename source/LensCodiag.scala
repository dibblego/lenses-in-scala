def codiag[A]: Lens[Either[A, A], A] = {
  val id = Lens[A, A](identity, (_, a) => a)
  id ||| id
}
