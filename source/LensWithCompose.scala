case class Lens[R, F](
  get: R => F
, set: (R, F) => R
) {
  def compose[Q]
    (g: Lens[Q, R])
    : Lens[Q, F] =
      Lens(
        get = 
          get compose g.get
      , set = (q, f) => 
          g set (q, set(g get q, f))
      ) 
}
