case class State[S, A](run: S => (A, S)) {
  def map[B](f: A => B): State[S, B] =
    State(s => {
      val (a, t) = run(s)
      (f(a), t)
    })

  def flatMap[B]
    (f: A => State[S, B])
    : State[S, B] =
      State(s => {
        val (a, t) = run(s)
        f(a) run t
      })
}

case class CoState[F, R](
  get: F
, set: F => R
) {
  def map[S](f: R => S): CoState[F, S] =
    CoState(
      get
    , f compose set
    )

  def coFlatMap[S]
    (f: CoState[F, R] => S)
    : CoState[F, S] =
      CoState(
        get
      , k => f(CoState(k, set))
      )
}
