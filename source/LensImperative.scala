case class Lens[R, F](
  get: R => F
, set: (R, F) => R
) {  
  def +=(n: F)
    (implicit m: Numeric[F])
    : State[R, F] =
      State(r => {
        val w = m.plus(get(r), n)
        (w, set(r, w))
      })

  def :=(f: => F): State[R, F] =
    State(r => (f, set(r, f)))
}
