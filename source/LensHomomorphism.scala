def homomorphism[A, B]
    : FLens[A, B] => PartialLens[A, B] =
  l =>
    PartialLens(a => Some(l apply a))