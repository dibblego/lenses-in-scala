trait Category[~>[_, _]] {
  def compose[A, B, C]
    (f: B ~> C)
    (g: A ~> B)
    : A ~> C
  def id[A]: A ~> A
}
