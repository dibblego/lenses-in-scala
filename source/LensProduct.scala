case class Lens[R, F](
  get: R => F
, set: (R, F) => R
) {
  def ***[S, G]
    (y: Lens[S, G])
    : Lens[(R, S), (F, G)] =
      Lens(
        rs => (get(rs._1), y get rs._2)
      , (rs, fg) => 
          (
            set(rs._1, fg._1)
          , y set (rs._2, fg._2)
          )
      )
}
