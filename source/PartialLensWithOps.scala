case class PartialLens[T, C](
  apply: T => Option[CoState[C, T]]
) {
  // Partial lens category composition
  def compose[D](f: PartialLens[C, D])
  	  : PartialLens[T, D] =
  	PartialLens(t => for {
  	  c <- this apply t
  	  d <- f apply c.get
  	} yield CoState(
  	  d.get
  	, x => c set (d set x)
  	))
  // Alternate on a choice  	
  def |||[U](f: PartialLens[U, C])
      : PartialLens[Either[T, U], C] =
    PartialLens {
   	  case Left(a) => 
   	    apply(a) map (x => CoState(
   	      x.get
   	    , w => Left(x set w)
   	    ))
   	  case Right(b) =>
   	    f apply b map (y => CoState(
   	      y.get
   	    , w => Right(y set w)
   	    ))
    }
  // Product of disjoint lenses  
  def ***[U, D](f: PartialLens[U, D])
      : PartialLens[(T, U), (C, D)] =
    PartialLens {
      case (t, u) =>
        for {
          x <- apply(t)
          y <- f apply u
        } yield CoState(
          (x.get, y.get)
        , w => (x set w._1, y set w._2)
        )
    }
  // State value for monadic programming     
  def state: State[T, Option[C]] =
    State(t => (apply(t) map (_.get), t))
  // Modify constructor values
  def modify(f: C => C): T => T =
    t => apply(t) match {
      case None    => t
      case Some(w) => w.set(f(w.get))   
    }
}
object PartialLens {
  // Partial lens category identity
  def id[A]
      : PartialLens[A, A] =
    PartialLens(a => Some(CoState(a, identity)))
  // Partial lens codiagonal
  def codiag[A]
      : PartialLens[Either[A, A], A] =
    id ||| id
}