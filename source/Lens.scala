case class Lens[R, F](
  get: R => F
, set: (R, F) => R
)
