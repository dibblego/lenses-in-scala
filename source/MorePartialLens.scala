// the head of a list
def listHeadPartialLens[A]
    : PartialLens[List[A], A] =
  PartialLens {
    case Nil => None
    case h :: t => Some(CoState(h, _ :: t))
  }

// the Some value of an Option
def somePartialLens[A]
    : PartialLens[Option[A], A] =
  PartialLens(_ map (z => 
  	CoState(z, Some(_))))

// the array of a JSON value 
// (util.parsing.json)
def scalaJSONArrayPartialLens[A]
    : PartialLens[JSONType, List[Any]] =
  PartialLens {
    case JSONArray(a) => 
   	  Some(CoState(a, JSONArray(_)))
    case _            =>
      None
  }