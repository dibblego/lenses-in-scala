case class Address(
  street: String
, state:  String
)

case class Person(
  age:     Int
, address: Address
)
