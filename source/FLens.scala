case class CoState[F, R](
  get: F
, set: F => R
)

case class FLens[R, F](
  apply: R => CoState[F, R]
)
