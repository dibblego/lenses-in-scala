case class Lens[R, F](
  get: R => F
, set: (R, F) => R
) {
  def |||[S, G]
    (y: Lens[S, F])
    : Lens[Either[R, S], F] =
      Lens({
        case Left(r)  => get(r)
        case Right(s) => y get s
      }
      , { 
        case (Left(r), f)  => 
          Left(set(r, f))
        case (Right(s), f) => 
          Right(y.set(s, f))
      })
}
