val ageL: Lens[Person, Int] =
  Lens(
    get = _.age
  , set = (p, a) => p.copy(age = a)
  )
