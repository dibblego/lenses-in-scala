case class PartialLens[T, C](
  apply: T => Option[CoState[C, T]]
)