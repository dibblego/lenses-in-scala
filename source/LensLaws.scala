object LensLaws {
  def law1[R, F]
    (r: R, f: F)
    (lens: Lens[R, F]) =
      lens.get(lens.set(r, f)) == f

  def law2[R, F](r: R)(lens: Lens[R, F]) =
    lens.set(r, lens get r) == r

  def law3[R, F]
    (r: R, f1: F, f2: F)
    (lens: Lens[R, F]) =
      lens.set(lens.set(r, f2), f1) == 
        lens.set(r, f1)
}
