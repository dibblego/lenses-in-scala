\section{Asymmetric Lenses}
\label{sect:asymmetriclenses}

\subsection{Canonical Representation}
\label{subsect:canonicalrepresentation}

An asymmetric lens is given by a binary type constructor defined over the record type (R) \textemdash the primary component in the bidirectional relationship \textemdash and the field type (F). The lens structure is the product of the accessor and updater function:
\lstinputlisting[label=lst:Lens.scala,caption=Lens.scala]{source/Lens.scala}

We would then denote the lens of a \lstinline$Person$ to its \lstinline$age$ with a value of the \lstinline$Lens$ type:

\lstinputlisting[label=lst:ageLens,caption=ageLens]{source/AgeLens.scala}

We are now able to derive a \lstinline$modify$ function that operates on any lens.
\begin{lstlisting}[label=lst:Lensmodify,caption=The \lstinline$modify$ function derivable from any \lstinline$Lens$,language=Scala]
def modify[R, F]
  (l: Lens[R, F])
  (f: F => F)
  : R => R =
    r => l set (r, f(l get r))
\end{lstlisting}

\subsection{Lens Laws}
\label{subsect:lenslaws}

Lens implementations are expected to satisfy three laws (invariants). Adherence to these laws provides guarantees about the behaviour of higher-level libraries on lenses. The Scala type-checker will not enforce these laws so adherence is delegated to the programmer's responsibility.

\begin{enumerate}
\item If you set a field, then get that field, you have the originally set field value:
\begin{lstlisting}[mathescape,language=Scala]
$\forall$ r f. lens.get(lens.set(r, f)) == f
\end{lstlisting}
\item If you get a field, then set that field, you have the same record value:
\begin{lstlisting}[mathescape,language=Scala]
$\forall$ r. lens.set(r, lens get r) == r
\end{lstlisting}
\item If you set a field twice with values \lstinline$f2$ then \lstinline$f1$, this is the same as setting it once with value \lstinline$f1$. The
first set operation of \lstinline$f2$ is not observable. 
\begin{lstlisting}[mathescape,language=Scala]
$\forall$ r f1 f2.
  lens.set(lens.set(r, f2), f1) ==
    lens.set(r, f1)
\end{lstlisting}
\end{enumerate}

\newpage

We can express these laws in Scala. All the following functions should return \lstinline$true$, regardless of the value of their arguments:

\lstinputlisting[label=lst:LensLaws,caption=Lens laws expressed in Scala]{source/LensLaws.scala}

\subsection{Lenses are Categories}
\label{subsect:lenscategory}

Perhaps the first interesting observation about a \lstinline$Lens$ is that it forms a category\cite{awodey2006category}. We take a category by the following interface:

\lstinputlisting[label=lst:Category,caption=The Category trait]{source/Category.scala}

We implement the \lstinline$Category$ trait for \lstinline$Lens$ by the following implementation:

\lstinputlisting[label=lst:LensCategory,caption=The Category instance for Lens]{source/LensCategory.scala}

\subsection{Lenses Compose}
\label{subsect:lensescompose}

We can see immediately that since a \lstinline$Lens$ composes in the same way that \lstinline$scala.Function1$ composes\footnote{See the \lstinline$scala.Function1\#compose$ method for its similarities \textemdash it too, forms a category.}, we can produce a new \lstinline$Lens$ for embedded data structures by composing each lens for each level of embedding. We add a method \lstinline$compose$ representing composition under the lens category:

\lstinputlisting[label=lst:LensWithCompose,caption=Lens with \lstinline$compose$ method]{source/LensWithCompose.scala}

\subsection{Exploiting Lens Composition}
\label{subsect:exploitingcomposition}

An example of exploiting the composition of lenses is to produce the lens for
the street of a person's address\footnote {Recall that a person has an address
and an address has a street.}. We may first take the address lens, which has the
type \lstinline$Lens[Person, Address]$. We then take the street lens, which has the type \lstinline$Lens[Address, String]$ and we can produce a lens with the type \lstinline$Lens[Person, String]$ by exploiting composition:

\begin{lstlisting}[label=lst:Lens_composition,caption=Lenses under composition,language=Scala]
def addressL: Lens[Person, Address] = ...
def streetL: Lens[Address, String] = ...
val personStreetL: Lens[Person, String] =
  streetL compose addressL
\end{lstlisting}

Using the \lstinline$personStreetL$ lens, we may access or set the (indirect) street property of a \lstinline$Person$ instance.

\begin{itemize}
\item \begin{lstlisting}[label=lst:personStreetL_get,language=Scala]
val str: String =
  personStreetL get person
\end{lstlisting}
\item \begin{lstlisting}[label=lst:personStreetL_set,language=Scala]
val newP: Person =
  personStreetL set (person, "Bob St")
\end{lstlisting}
\end{itemize}

Other interesting functions arise as a consequence of the lens category. For example, since all categories are monoids, we may reduce a list of lenses:

\begin{lstlisting}[label=lst:Lens_fold,caption=Fold a list of lenses,language=Scala]
def foldLens[A]
  (x: List[Lens[A, A]])
  : Lens[A, A] =
    x.foldRight(lensCat.id)(_ compose _)  
\end{lstlisting}

