----------------------- REVIEW 1 ---------------------
PAPER: 7
TITLE: Asymmetric Lenses in Scala
AUTHORS: Tony Morris

Summary:

This paper introduces a small lenses library in Scala and 
argues about the usefulness of lenses for various programming 
tasks in Scala. The paper starts by describing some existing Scala 
mechanisms for the manipulation of fields and points out limitations 
to those mechanisms. Then lenses are introduced with an 
emphazis on the compositionality properties that lenses have. 
The author shows that lenses allow us to overcome the limitations 
of Scala's built-in mechanisms. Later sections of the paper 
introduce a variation of lenses that has some additional benefits 
(such as performance); and describes various operations in the Lens 
library. 

Evaluation:

+ Nicely written paper; it presents, in a very readable way, 
the general idea of lenses and why they are useful. 

+ The proposed library is simple, elegant and useful.

Cons:

- I think the point about asymetric lenses in Section 4 
was not properly developed;

- Minor things: there should be a few more references; 
also, some points would be clearer with additional examples. 

I think this paper should be accepted. Lenses can be useful in
practice and this paper make a good case for them. Plus I found the
presentation to be very accessible. My complains are mostly minor.

Detailed comments:

- Missing references:

One reference that you should definitly mention is the 
original work on lenses: 

@article{Foster:2007:CBT:1232420.1232424,
 author = {Foster, J. Nathan and Greenwald, Michael B. and Moore, Jonathan T. and Pierce, Benjamin C. and Schmitt, Alan},
 title = {Combinators for bidirectional tree transformations: A linguistic approach to the view-update problem},
 journal = {ACM Trans. Program. Lang. Syst.},
 issue_date = {May 2007},
 volume = {29},
 issue = {3},
 month = {May},
 year = {2007},
 issn = {0164-0925},
 articleno = {17},
 url = {http://doi.acm.org/10.1145/1232420.1232424},
 doi = {http://doi.acm.org/10.1145/1232420.1232424},
 acmid = {1232424},
 publisher = {ACM},
 address = {New York, NY, USA},
 keywords = {Bidirectional programming, Harmony, XML, lenses, view update problem},
} 

It would also be nice to compare your work with some other 
approaches using lenses in Scala:

@inproceedings{sle11-wider,
  author = 	    {Wider, A.},
  title = 	    {Towards Combinators for Bidirectional Model Transformations in Scala},
  booktitle = 	    {Post-Proceedings of the 4th International Conference on Software Language Engineering (SLE'11), Braga, Portugal, July 3-4, 2011},
  volume =          {6940}
  series =          {LNCS}
  pages =           {??},
  editor =          {Assmann, Uwe and Sloane, Anthony}
  publisher = 	    {Springer},
  year =            {2011}
}

You could also convert footnotes 10, 11, 12 and 13 to references.

- Asymetric Lenses: 

You introduce an alternative representation for lenses (FLens) in Section 4
and argue that this representation is better for programming in Scala. 
But in the next sections, you apparently revert back to the original "Lens"
datatype. 

I was expecting later sections to be using "FLens", somehow. Would the
code in later sessions be essentially the same with FLens? What would
need to change? I think some additional discussion is needed.

Or maybe I missed something and FLens is used implicitly ...

- More examples:

The paper does a nice job showing small example applications for each
concept in the early parts of the paper. I would have appreciated a
couple more of such examples later on. For example:

  - An example illustrating the use of FLens.
  - Examples illustrating Lens product, choice and codiagonal morphisms.

Typos and small comments:

Section 4: I don't understand "(pedagogy)" in "obviates the existence
of ...". Could you be more explicit?

Listing 18: "ismorphism" --> "isomorphism"
Also, is this listing really necessary? I think we believe in the
mathmatical equivalence. 

Just before Listing 29: "seen5.6" --> "seen in Section 5.6"

