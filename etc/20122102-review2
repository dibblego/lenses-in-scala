----------------------- REVIEW 2 ---------------------
PAPER: 7
TITLE: Asymmetric Lenses in Scala
AUTHORS: Tony Morris

Though somewhat wordy, the text is nonetheless clear and easy to follow.  That's especially important since a goal of the paper is to introduce the subjects it covers.

My only strong reservation about this paper is that it lacks citations.  There are multiple mentions of "extensively studied" this, "canonical" that, "typically expected" implementations, and so forth, and though there are footnotes with references to concrete software projects, but there is only one reference to a previous written publication.  My "accept" recommendation is conditional on that being fixed.  Who originated the term and concept, "lens"?  Same question about the "asymmetric lens", the lens laws, the combination of lenses with the state monad (the "typical lens/state libraries" mentioned in section 5.6), and so on.

I do have some suggestions for improvements in the presentation, none of them critical to a thumbs up/down decision:

- Some readers are coming to Scala not from the FP side, but from languages like Java and Ruby in which mutable data is the norm.  It isn't this paper's job to explain everything about FP to these people, but I think the introduction should have some signposts indicating to those readers that the they're entering an unfamiliar pure-FP land in which everything is immutable and mutability is totally out of the picture from the start.  So I suggest the introduction should state at the outset that we're only talking about immutable values, so not case classes in general, but only case classes without vars, and that the copy method isn't an "updater" function in the sense of mutating, and so on.

- In Section 6.2, it would be interesting to know what general capability is missing in the current Scala Macros implementation that leaves it unable to perform the specific mission of generating lenses.  Without that, "The implementation does not yet have the ability" is an unsubstantiated claim.

- Shouldn't section 6.3 come before sections 6.1 and 6.2?

My recommendation for this paper is "accept".  If the paper had addressed the following concern, and addressed it well, then it could have been a "strong accept":

- Section 5.6 succeeds at showing that Lens and State can be combined to emulate ordinary imperative programming.  Readers already sold on the value of pure-FP may be impressed; other readers won't and without further material to convince them, they're likely to conclude that lenses only provide a roundabout way to accomplish they already knew how to accomplish more straightforwardly.  The author refers to standard arguments about the benefits of FP by using phrases such as "reason equationally", "referential transparency", "composable", and so forth.  At minimum, it would be good to direct readers to references where they can read more -- an obvious standard references would be Hughes, "Why Functional Programming?"  But it would be even better if the paper included at least one example that actually demonstrates some concrete advantages of using Lens and State instead of the ordinary imperative code they replace.

