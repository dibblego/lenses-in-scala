\section{Representing an Asymmetric Lens in Scala}
\label{sect:representinglens}

So far, we have denoted a lens by the product of the \lstinline$get$ and \lstinline$set$ functions.

There exists an alternative representation of asymmetric lenses which exploits a fusion of the target object onto the pair of the \lstinline$get$ and \lstinline$set$ functions \textemdash assuming a high likelihood that both \lstinline$get$ and \lstinline$set$ will be called on a given target object. This representation was first described by Russell O'Connor\cite{DBLP:journals/corr/abs-1103-2841}. Although this alternative representation is \emph{isomorphic} to our canonical representation and one can replace the other without affecting client code, it provides a couple of useful benefits:

\begin{itemize}
\item obviates the existence of particular library functions on structures used in the representation. The \lstinline$CoState$ data structure \textemdash used for representation \textemdash provides many library utilities, including a comonadic interface. 
\item suited to the Scala programming environment by providing a performance enhancement as a consequence of the fusion on the target object, increasing the viability of lenses on the JVM. In particular, since the application to the target object occurs once, we have a more efficient \lstinline$modify$ operation by having direct access to \lstinline$get$ and \lstinline$set$ closed over the target object. Many higher-level lens libraries utilise the \lstinline$modify$ operation and so would acquire this benefit.
\end{itemize}

\begin{figure}[H]
\begin{tikzpicture}

\draw[densely dotted] (0, 0) node {$\otimes$} -- (0, 0.5) -- (0.5, 0.5) node[right] {$A \otimes B \rightarrow A$};
\draw[densely dotted] (0, 0) -- (0, -0.5) -- (0.5, -0.5) node[right] {$A \rightarrow B$};

\end{tikzpicture}
\caption{Canonical representation of an asymmetric lens.}
\label{fig:canonicallens}
\end{figure}

This described alternative lens representation is first arrived at by currying the \lstinline$set$ function.

\begin{figure}[H]
\begin{tikzpicture}

\draw[densely dotted] (0, 0) node {$\otimes$} -- (0, 0.5) -- (0.5, 0.5) node[right] {$A \rightarrow B \rightarrow A$};
\draw[densely dotted] (0, 0) -- (0, -0.5) -- (0.5, -0.5) node[right] {$A \rightarrow B$};

\end{tikzpicture}
\caption{Intermediate step in an alternative representation of an asymmetric lens. The \lstinline$set$ operation is curried.}
\label{fig:intermediatealternativelens}
\end{figure}

We can see in this intermediate step that there is a common argument \textemdash the target object \textemdash to both function pairs. Let us now fuse the pair of functions on this argument.

\begin{figure}[H]
\begin{tikzpicture}

\node[left](T) {$A \rightarrow$};
\draw[densely dotted] (0, 0) node {$\otimes$} -- (0, 0.5) -- (0.5, 0.5) node[right] {$B \rightarrow A$};
\draw[densely dotted] (0, 0) -- (0, -0.5) -- (0.5, -0.5) node[right] {$B$};

\end{tikzpicture}
\caption{Alternative representation of an asymmetric lens fused on the target object.}
\label{fig:fusedlens}
\end{figure}

\subsection{Fusing the Target of a Lens}
\label{subsect:fusingtarget}

This alternative representation provides the user with an interface where the target object is passed and a pair denoting the \lstinline$get$ and \lstinline$set$ functions is returned, closed over the target object. We will call this pair of functions, \lstinline$CoState$.

\newpage

\lstinputlisting[label=lst:FLens.scala,caption=An asymmetric lens fused on the target object]{source/FLens.scala}

\subsection{A Lens Fused on its Target is Isomorphic}

To demonstrate that this alternative representation has not altered the algebraic structure of the lens, the isomorphism is given by the following bijection:
\lstinputlisting[label=lst:LensIso.scala,caption=The isomorphism of the two lens representations]{source/LensIso.scala}

Further, by exploring the algebraic representation of each structure, we can observe an equivalence by specification testing.

Under the usual algebraic correspondence, taking \lstinline$=>$ to denote exponentiation and pairing to denote multiplication, the canonical representation of a lens gives us:
\[
{F^R} \times {R^{F \times R}}
\]
Similarly, for our alternative representation, we have:
\[
{\left( F \times {R^F} \right) }^R
\]

We can assert this using ScalaCheck\footnote{\url{http://code.google.com/p/scalacheck/}}:

\begin{lstlisting}[label=lst:ScalaCheckIso,caption=Verifying the isomorphism with ScalaCheck,language=Scala]
forAll((r: Double, f: Double) => 
  (r >= 0 && f >= 0) ==> {
    val p = math.pow(_: Double, _: Double)
    val canonical = p(f, r) * p(r, f * r)
    val alternative = p(f * p(r, f), r)
    canonical == alternative
  }
).check
\end{lstlisting}
\lstinline$+ OK, passed 100 tests.$

We will revert to using \lstinline$Lens$ from hereon, keeping in mind that \lstinline$FLens$ is substitutable by the bijection.

\subsection{CoState is a Comonad}

\lstinline$CoState$ is so named because it is the \emph{categorical dual} of the relatively familiar \lstinline$State$ data structure\cite{jones1995functional}. Just as \lstinline$State$ is a (particularly useful) monad and so gives rise to \lstinline$flatMap$ and \lstinline$map$ methods, so too \lstinline$CoState$ is a comonad\cite{DBLP:journals/corr/abs-1103-2841}, giving rise to \lstinline$coFlatMap$ \footnote{so named to keep with the Scala convention of \lstinline$flatMap$ but the type signature has "the arrows reversed"} and \lstinline$map$ methods.

\lstinputlisting[label=lst:StateCoState.scala,caption=State monad and CoState comonad]{source/StateCoState.scala}

As a consequence of this comonad, we have access to libraries that naturally arise. For example, we are able to run the co-flatten\footnote{\lstinline$coFlatten: F[A] => F[F[A]]$ in contrast to \lstinline$flatten: F[F[A]] => F[A]$} operation \textemdash the dual of the more familiar \lstinline$flatten$ operation in Scala \textemdash on a \lstinline$CoState$ instance:

\begin{lstlisting}[label=lst:CoStateCoFlatten,caption=Comonadic duplication of the get and set pair of \lstinline$CoState$,language=Scala]
def coFlatten[F, R]
  (s: CoState[F, R])
  : CoState[F, CoState[F, R]] =
    s coFlatMap identity
\end{lstlisting}

\newpage

