\section{Partial Lenses}
\label{sect:partiallenses}

Just as regular lenses correspond to fields of record types, \emph{partial lenses} correspond to constructors of sum types. Partial lenses are represented by a function accepting the target object and \emph{optionally} returning a value with the type \lstinline$CoState[C, T]$ where \lstinline$C$ denotes the type of the constructor's arguments and \lstinline$T$ denotes the type that the constructor produces.

For example, the \lstinline$Option$ partial lens defined for the \lstinline$Some$ constructor produces (an optional) value of the type \lstinline$CoState[A, Option[A]]$ since the \lstinline$Some$ constructor accepts a value of the type \lstinline$A$ to construct a value of the type \lstinline$Option[A]$. This partial lens value is implemented further on in listing \ref{lst:MorePartialLens.scala}.  

\lstinputlisting[label=lst:PartialLens.scala,caption=Partial Lens data structure]{source/PartialLens.scala}

\newpage

Just as a mundane example of the use of regular lenses is for the most basic product type, \lstinline$Tuple2$ and its \lstinline$_1$ and \lstinline$_2$ fields, a similar example of the use of partial lenses is for the most basic sum type, \lstinline$Either$ and its \lstinline$Left$ and \lstinline$Right$ constructors.

\lstinputlisting[label=lst:LeftRightPartialLens.scala,caption=Partial Lens for constructors of \lstinline$Either$]{source/LeftRightPartialLens.scala}

Informally, we may encounter problem questions in the field such as:

\begin{itemize}
\item ``How do I update the left value of my Either?''
\item ``How do I update the head of my list?''
\item ``How do I modify the first element of my JSON Array?''
\end{itemize}

Of course, there is not necessarily a left value of an \lstinline$Either$ (it may be \lstinline$Right$) or there may not be a head of a list (it may be empty), however, partial lenses allow us to program in such a way as if those values did exist, with the partial lens itself taking care of the possibility that it might not. Indeed, partial lenses provide an elegant answer to the aforementioned questions. 

\newpage

Following are more interesting partial lens values defined on data types that are integral to the Scala standard library \textemdash some oriented toward the above questions:

\lstinputlisting[label=lst:MorePartialLens.scala,caption={Partial Lens for list head, \lstinline$Option Some$, JSON array}]{source/MorePartialLens.scala}

Partial lenses exhibit many of the same properties of regular lenses:

\begin{itemize}
\item Partial lenses form a category, with the ability to compose and an identity value. {$(P \rightsquigarrow Q) \rightarrow (Q \rightsquigarrow R) \rightarrow (P \rightsquigarrow R)$} and {$(P \rightsquigarrow P)$}
\item Partial lenses can alternate on a choice. {$(R \rightsquigarrow F) \rightarrow (S \rightsquigarrow F) \rightarrow (R \oplus S \rightsquigarrow F)$}
\item Partial lenses combine on the product of disjoint values. {$(R \rightsquigarrow F) \rightarrow (S \rightsquigarrow G) \rightarrow (R \otimes S \rightsquigarrow F \otimes G)$}
\item Partial lenses give rise to a codiagonal morphism. {$A \oplus A \rightsquigarrow A$}
\item Partial lenses give rise to a \lstinline$State$ value, useful for monadic programming, for which Scala has syntactic support. 
\item Partial lenses exhibit a \lstinline$modify$ operation, for running modifications on constructor values.
\end{itemize}

\newpage

Implementations for these operations follow.

\lstinputlisting[label=lst:PartialLensWithOps.scala,caption={Partial Lens with common lens operations}]{source/PartialLensWithOps.scala}

Given a regular lens, it is (trivially) possible to produce a partial lens by putting the \lstinline$CoState$ value in the \lstinline$Some$ constructor. That is to say, there exists a \emph{homomorphism} from the \lstinline$Lens$ category to the \lstinline$PartialLens$ category. This is particularly useful for working with partial lenses and incorporating lenses on record types e.g. to compose with them. 

\lstinputlisting[label=lst:LensHomomorphism.scala,caption={PartialLens $\rightarrow$ Lens homomorphism}]{source/LensHomomorphism.scala}

